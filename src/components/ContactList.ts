import { Bot } from '../bots/Bot';
import { Bot1 } from '../bots/Bot1';
import { Bot2 } from '../bots/Bot2';

export class ContactList {
  private container: HTMLElement;
  private onSelectBot: (bot: Bot) => void;
  private selectedBotButton: HTMLButtonElement | null = null;

  constructor(container: HTMLElement, onSelectBot: (bot: Bot) => void) {
    this.container = container;
    this.onSelectBot = onSelectBot;
  }

  render() {
    this.container.innerHTML = `
      <div class="contact-list">
        <button id="bot1" class="btn btn-outline-primary">Bot1</button>
        <button id="bot2" class="btn btn-outline-secondary">Bot2</button>
      </div>
    `;

    const bot1Button = document.getElementById('bot1') as HTMLButtonElement;
    const bot2Button = document.getElementById('bot2') as HTMLButtonElement;

    bot1Button?.addEventListener('click', () => {
      this.selectBot(bot1Button, new Bot1());
    });

    bot2Button?.addEventListener('click', () => {
      this.selectBot(bot2Button, new Bot2());
    });
  }

  private selectBot(button: HTMLButtonElement, bot: Bot) {
    if (this.selectedBotButton) {
      this.selectedBotButton.classList.remove('active');
    }
    this.selectedBotButton = button;
    this.selectedBotButton.classList.add('active');
    this.onSelectBot(bot);
  }
}
